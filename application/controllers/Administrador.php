<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

	function __construct(){
		parent::__construct();
		//verify_session_admin();
	}

	public function index()
	{
		$data['title'] = 'Registro';
		$data['productos'] = $this->cargarProductos();

		$this->load->view('include/header', $data);
		$this->load->view('admin/panel');
		$this->load->view('include/footer');
	}

	public function cargarProductos(){
		$productos = $this->db->select("*")->from("productos")->get()->result();
		return $productos;
	}

	public function crearProducto(){
		$nombreProducto = $this->input->post('nombreProducto');
		$precioProducto = $this->input->post('precioProducto');
		$descripcionProducto = $this->input->post('descripcionProducto');
		$existencia = $this->input->post('existencia');

		$name_random = random(10);
		$size = 10000000;

		$nombre_imagen =  "producto_".$name_random.$_FILES['file']['name'];
		$tipo_imagen = pathinfo($nombre_imagen, PATHINFO_EXTENSION);

		if(0 < $_FILES['file']['error']) {
		    echo json_encode(array('msg'=>"Hubo un error al actualizar, intente de nuevo."));
		}else if($_FILES['file']['size'] > $size){
			 echo json_encode(array('msg'=>"El tamaño supera 10 MB, intente con otro png."));
		}else if($tipo_imagen != "png"){
			echo json_encode(array('msg'=>"La extensión de la imagen no es correcta, debe ser png (archivo.png)"));
		}else if(move_uploaded_file($_FILES['file']['tmp_name'], 'assets/img/productos/'.$nombre_imagen)){

			$data_producto = array(
				'nombre' => $nombreProducto,
				'imagen' => $nombre_imagen,
				'precio' => $precioProducto,
				'descripcion' => $descripcionProducto,
				'existencia' => $existencia
			);

			if($this->db->insert('productos', $data_producto)){
				echo json_encode(array('msg' => "Producto Agregado..."));
			}	
		}
	}

	public function eliminarProducto(){
		$id = $this->input->post('id');

		if($this->db->where('id', $id)->delete('productos')){
			echo json_encode(array('msg' => "Producto eliminado..."));
		}

	}

}

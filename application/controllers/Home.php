<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Home';

		$this->load->view('include/header_home', $data);
		$this->load->view('home');
		$this->load->view('include/footer');
	}
	public function registro()
	{
		$data['title'] = 'Registro';

		$this->load->view('include/header', $data);
		$this->load->view('auth');
		$this->load->view('include/footer');
	}

	public function administrador(){
		$data['title'] = 'Administrador';

		$this->load->view('include/header', $data);
		$this->load->view('admin/login');
		$this->load->view('include/footer');
	}

	function email(){
    	$emailTo = $this->input->post('email');
    	$amigo = $this->input->post('amigo');
    	$item_number = $this->input->post('item_number');
    	$txn_id = $this->input->post('txn_id');
    	$status = $this->input->post('status');
    	$payment_amt = $this->input->post('payment_amt');
    	$currency_code = $this->input->post('currency_code');
    	$serial = random(8);

    	$productoInfo = $this->db->select("*")->from("productos")->where("id", $item_number)->get()->row();
    	$nombreProducto = $productoInfo->nombre;
    	$imagen = $productoInfo->imagen;

		$from = "QShop";
		$asunto = "Cupón";
		$num_prioridad = 1;

		$mensaje = "Hola, de parte de QShop te agradecemos en comprar nuestros productos. Acontinuación te doy la informacion del cupón: \n Número del Cupón: ".$serial.".\n Nombre: ".$nombreProducto."\n <img src='".base_url("assets/img/$imagen").">";

		$this->email->from("marsarmy@outlook.com", $from);
		$this->email->to($emailTo);
		$this->email->subject('QShop - : '.$asunto);
		$this->email->set_priority($num_prioridad);

		$data_msg['mensaje'] = $mensaje;

		$email_view = $this->load->view('email/contacto', $data_msg, true);

		$this->email->message($email_view);

		if($this->email->send()){

			$dataUser = $this->db->select("*")->from("users")->where("email", $emailTo)->get()->row();
			
			$userId = $dataUser->oauth_uid;

			$data_pay = array(
				'user_id' => $userId,
				'product_id' => $item_number,
				'txn_id' => $txn_id,
				'payment_gross' => $payment_amt,
				'currency_code' => $currency_code,
				'payer_email' => $emailTo,
				'payment_status' => "Pendiente",
			);

			if($this->db->insert('payments', $data_pay)){
				$data_cupon = array(
					'serial' => $serial,
					'status' => "Pendiente",
					'user_id' => $userId,
					'amigo' => $amigo
				);

				if($this->db->insert('cupones', $data_cupon)){
					echo json_encode(array('msg'=>"Cupón registrado."));
				}
			}
		}else{
			echo json_encode(array('url'=>"login", 'msg'=>"Lo sentimos, hubo un error y no se envio el correo."));
		}
	}
}

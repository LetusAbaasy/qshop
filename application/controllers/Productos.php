<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Productos extends CI_Controller
{
	function  __construct() {
		parent::__construct();
		// Load facebook library
		$this->load->library('facebook');
		$this->load->library('paypal_lib');
		$this->load->model('ProductosM');
	}
	
	function index(){
		$data = array();
		$data['title'] = 'Tienda';
		if($this->facebook->is_authenticated()){
			$data['fb'] = true;
		}else{
			$data['fb'] = false;
		}

		$this->load->view('include/header', $data);
		//get products data from database
        $data['productos'] = $this->ProductosM->getRows();
		//pass the products data to view
		$this->load->view('productos/index', $data);
		$this->load->view('include/footer');
	}
	
	function buy($id){
		//Set variables for paypal form
		$returnURL = base_url().'paypal/success'; //payment success url
		$cancelURL = base_url().'paypal/cancel'; //payment cancel url
		$notifyURL = base_url().'paypal/ipn'; //ipn url
		//get particular product data
		$producto = $this->ProductosM->getRows($id);
		$userID = 1; //current user id
		$logo = base_url().'assets/img/favicon.ico';
		
		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notifyURL);
		$this->paypal_lib->add_field('item_name', $producto['nombre']);
		$this->paypal_lib->add_field('custom', $userID);
		$this->paypal_lib->add_field('item_number',  $producto['id']);
		$this->paypal_lib->add_field('amount',  $producto['precio']);		
		$this->paypal_lib->image($logo);
		
		$this->paypal_lib->paypal_auto_form();
	}

	public function verProducto(){
		$info = $this->input->get();
		$id = $info['id']; 

        $producto = $this->db->select("*")->from("productos")->where("id", $id)->get()->row();
        foreach ($producto as $p) {
			$data['id'] = $producto->id;
			$data['nombre'] = $producto->nombre;
			$data['imagen'] = $producto->imagen;
			$data['precio'] = $producto->precio;
			$data['status'] = $producto->status;
			$data['descripcion'] = $producto->descripcion;
			$data['existencia'] = $producto->existencia;
			$data['compras'] = $producto->compras;
        }
        if($this->facebook->is_authenticated()){
			$data['fb'] = true;
		}else{
			$data['fb'] = false;
		}
		$data['title'] = 'Producto';
		$this->load->view('include/header', $data);
		$this->load->view('productos/producto', $data);
		$this->load->view('include/footer');

	} 
}
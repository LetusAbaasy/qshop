<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProductosM extends CI_Model{
	//get and return product rows
	public function getRows($id = ''){
		$this->db->select('id,nombre,imagen,precio,descripcion,existencia,compras');
		$this->db->from('productos');
		if($id){
			$this->db->where('id',$id);
			$query = $this->db->get();
			$result = $query->row_array();
		}else{
			$this->db->order_by('nombre','asc');
			$query = $this->db->get();
			$result = $query->result_array();
		}
		return !empty($result)?$result:false;
	}
	//insert transaction data
	public function insertTransaction($data = array()){
		$insert = $this->db->insert('payments',$data);
		return $insert?true:false;
	}
}
<!--Main layout-->
    <main class="pb-0">
        <div class="container">
            <!--First row-->
            <div class="row mt-5 pt-lg-4 wow fadeIn">
                <div class="col-lg-7">
                    <!--Featured image -->
                    <div class="view overlay hm-white-light z-depth-1-half">
                        <img src="<?php echo base_url('assets/img/banner1.jpg'); ?>" class="img-fluid " alt="">
                        <div class="mask waves-effect waves-light">
                        </div>
                    </div>
                    <br>
                </div>

                <!--Main information-->
                <div class="col-lg-5 wow fadeIn">
                    <h2 class="h2-responsive font-bold dark-grey-text">¿Quienes somos?</h2>
                    <hr>
                    <p class="dark-grey-text mt-3">Q'Shop es una solución tecnológica que permite a un usuario realizar compras de productos online. “Para la construcción de la solución se usa la metodología de desarrollo ágil “Scrum”, la cual se adapta a las necesidades del proyecto, pues, es un proceso en el que se aplican un conjunto de buenas prácticas para trabajar colaborativamente, en equipo, y obtener el mejor resultado posible de un proyecto.”</p>
                    <a href="<?php echo base_url('tienda'); ?>" class="btn btn-indigo btn-md waves-effect waves-light">Ir a la tienda</a>
                </div>
            </div>
            <!--/.First row-->

            <hr class="extra-margins my-5">

            <!--Second row-->
            <div class="row pt-4 mb-4">
                <!--First columnn-->
                <div class="col-lg-4">
                    <!--Card-->
                    <div class="card mb-r wow fadeIn">

                        <!--Card image-->
                        <img class="img-fluid" src="<?php echo base_url('assets/img/banner1.jpg'); ?>" alt="Card image cap">

                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <h4 class="card-title text-center dark-grey-text">
                                <strong>Calidad</strong>
                            </h4>
                            <hr>
                            <!--Text-->
                            <p class="card-text">Con los mejores proveedores.</p>
                        </div>

                    </div>
                    <!--/.Card-->
                </div>
                <!--First columnn-->

                <!--Second columnn-->
                <div class="col-lg-4">
                    <!--Card-->
                    <div class="card mb-r wow fadeIn">

                        <!--Card image-->
                        <img class="img-fluid" src="<?php echo base_url('assets/img/img1.jpg'); ?>" alt="Card image cap">

                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <h4 class="card-title text-center dark-grey-text">
                                <strong>Servicio</strong>
                            </h4>
                            <hr>
                            <!--Text-->
                            <p class="card-text">Confiable y rapido.</p>
                        </div>

                    </div>
                    <!--/.Card-->
                </div>
                <!--Second columnn-->

                <!--Third columnn-->
                <div class="col-lg-4">
                    <!--Card-->
                    <div class="card mb-r wow fadeIn">

                        <!--Card image-->
                        <img class="img-fluid" src="<?php echo base_url('assets/img/banner1.jpg'); ?>" alt="Card image cap">

                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <h4 class="card-title text-center dark-grey-text">
                                <strong>Precios</strong>
                            </h4>
                            <hr>
                            <!--Text-->
                            <p class="card-text">Accesibles a todo mundo.</p>
                        </div>

                    </div>
                    <!--/.Card-->
                </div>
                <!--Third columnn-->
            </div>
            <!--/.Second row-->
        </div>

        <!--Second container-->
        <div class="container-fluid pb-0">
            <div class="container py-4">

                <!--Section: Services-->
                <section id="services" class="section mt-3 mb-3 pb-3">

                    <!-- Section heading -->
                    <h3 class="text-center title my-5 pt-4 pb-5 dark-grey-text font-bold wow fadeIn">
                        <strong>Nuestros servicios</strong>
                    </h3>

                    <!-- First row -->
                    <div class="row wow fadeIn">

                        <!-- First column -->
                        <div class="col-md-6 mb-r text-center">

                            <!--Panel-->
                            <div class="card card-body text-left white hoverable">
                                <p class="feature-title title font-bold dark-grey-text font-up spacing mt-4 mx-4">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                    <strong>Facebook</strong>
                                </p>
                                <p class="grey-text font-small mx-4">La API Graph es el modo principal para extraer e ingresar datos en la plataforma de Facebook. Se trata de una API basada en HTTP de nivel inferior que puedes utilizar de manera programática para consultar datos, publicar nuevas historias, administrar anuncios, subir fotos y llevar a cabo varias tareas más propias de una aplicación.
                                    </p>
                                    <!--<p class="font-small font-bold indigo-text mx-4 mb-0">
                                        <a>read more</a>
                                    </p>-->
                                <p></p>
                            </div>
                            <!--/.Panel-->

                        </div>
                        <!-- /First column -->

                        <!-- Third column -->
                        <div class="col-md-6 mb-r text-center">

                            <!--Panel-->
                            <div class="card card-body text-left white hoverable">
                                <p class="feature-title title font-bold dark-grey-text font-up spacing mt-4 mx-4">
                                    <i class="fa fa-tablet" aria-hidden="true"></i>
                                    <strong>Servicio</strong>
                                </p>
                                <p class="grey-text font-small mx-4">Contiene una serie de librerías que sirven para el desarrollo de aplicaciones web y además propone una manera de desarrollarlas que debemos seguir para obtener provecho de la aplicación. (...) implementa el proceso de desarrollo llamado Model View Controller (MVC), que es un estándar de programación de aplicaciones, utilizado tanto para hacer sitios web como programas tradicionales.
                                    </p>
                                    <!--<p class="font-small font-bold indigo-text mx-4 mb-0">
                                        <a>read more</a>
                                    </p>-->
                                <p></p>
                            </div>
                            <!--/.Panel-->

                        </div>
                        <!-- /.Third column -->

                        <!-- Second column -->
                        <div class="col-md-6 mb-r text-center">

                            <!--Panel-->
                            <div class="card card-body text-left white hoverable">
                                <p class="feature-title title font-bold dark-grey-text font-up spacing mt-4 mx-4">
                                    <i class="fa fa-cc-paypal" aria-hidden="true"></i>
                                    <strong>PayPal</strong>
                                </p>
                                <p class="grey-text font-small mx-4">PayPal Sandbox no es ni más ni menos que una copia de PayPal, todo funciona exactamente igual, salvo dos cosas:
                                Las cuentas de PayPal no pueden usarse con PayPal Sandbox, y viceversa.
                                Todo el dinero que se mueve en PayPal Sandbox es ficticio. 
                                No hay que especificar ni tarjetas de crédito ni nada por el estilo, todo son datos ficticios, de pruebas, y el dinero, de “juguete”. Como si pagaras en tu tienda con billetes del Monopoly.
                                Por lo demás, PayPal y PayPal Sandbox son clones. Además, PayPal Sandbox incorpora un panel de administración.</p>
                                    <!--<p class="font-small font-bold indigo-text mx-4 mb-0">
                                        <a>read more</a>
                                    </p>-->
                                <p></p>
                            </div>
                            <!--/.Panel-->

                        </div>
                        <!-- /.Second column -->

                        <!-- Fourth column -->
                        <div class="col-md-6 mb-r text-center">

                            <!--Panel-->
                            <div class="card card-body text-left white hoverable">
                                <p class="feature-title title font-bold dark-grey-text font-up spacing mt-4 mx-4">
                                    <i class="fa fa-info" aria-hidden="true"></i>
                                    <strong>Ayuda</strong>
                                </p>
                                <p class="grey-text font-small mx-4">Ayuda a las personas..
                                    </p>
                                    <!--<p class="font-small font-bold indigo-text mx-4 mb-0">
                                        <a>read more</a>
                                    </p>-->
                                <p></p>
                            </div>
                            <!--/.Panel-->

                        </div>
                        <!-- /.Fourth column -->

                    </div>
                    <!-- /.First row -->

                </section>
                <!--/Section: Services-->

            </div>

        </div>
        <!--Second container-->

    </main>
    <!--/.Main layout-->

<div class="hiddendiv common"></div>
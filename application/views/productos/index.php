<div class="container">
    <hr/>
    <h3 class="text-center">Tienda - Q'Shop</h3>
    <hr/>
    <section class="text-center mb-4">
        <div class="row wow fadeIn">
        <?php if(!empty($productos)){ foreach($productos as $producto): ?>
          <div class="col-lg-3 col-md-6 mb-4">
            <hr/>
            <div class="card">
              <div class="view overlay">
                <img src="<?php echo base_url().'assets/img/productos/'.$producto['imagen']; ?>" class="card-img-top" alt="">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <hr/>
              <div class="card-body text-center">
                <a href="" class="grey-text">
                  <!--<h5>Shirt</h5>-->
                </a>
                <h5>
                    <a href="" class="dark-grey-text">Descripción:
                    </a>
                      <br/>
                      <label class="text-justify pull-left"><?php echo $producto['descripcion']; ?></label>
                      <p class="text-justify pull-right">Existencia: <?php echo $producto['existencia']; ?></p>
                      <br/>
                    <span class="badge badge-pill danger-color">Nuevo</span>
                </h5>
                <hr/>
                <h4 class="font-weight-bold blue-text">
                  <strong>$<?php echo $producto['precio']; ?> USD</strong><br/>
                    <?php if($fb == true){ ?>
                      <?php if($producto['existencia'] > 0){ ?>
                        <a href="<?php echo base_url().'productos/buy/'.$producto['id']; ?>"><img src="<?php echo base_url(); ?>assets/img/x-click-but01.gif" style="width: 70px;"></a>
                        <small class="small pull-right"><?php echo $producto['compras']; ?> Compra(s)</small>
                      <?php }else{ ?>
                        <small>No hay existencias.</small>
                      <?php } ?>
                    <?php }else{ ?>
                      <small>Inicia sesión para comprar este producto.</small>
                    <?php } ?>
                </h4>
                <a href="tienda/verProducto/?id=<?php echo $producto['id']; ?>"><button class="btn btn-info btn-block">Ver <?php echo $producto['nombre']; ?></button></a>
                <hr/>
              </div>
            </div>
          </div>
            <?php endforeach;} else{?>
                <h4>No hay productos en la tienda.</h4>
            <?php } ?>
        </div>
    </section>
</div>
<div class="clearfix"></div>
<hr/>
<h3 class="text-center">Nuestros Aliados</h3>
<div id="carousel2" class="carousel slide">
    <ol class="carousel-indicators">
      <li data-target="#carousel2" data-slide-to="0" class="active"></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active"> 
        <img class="img-responsive" src="assets/img/slider/3.png" alt="thumb">
        <div class="carousel-caption"></div>
      </div>
    </div>
    <a class="left carousel-control" href="#carousel2" data-slide="prev"><span class="icon-prev"></span></a> <a class="right carousel-control" href="#carousel2" data-slide="next"><span class="icon-next"></span></a>
</div>
<div class="clearfix"></div>
<div class="container dark-grey-text mt-5">
      <div class="row wow fadeIn">
        <div class="col-md-6 mb-4">
          <img src="<?php echo base_url().'assets/img/productos/'.$imagen; ?>" class="img-fluid" alt="">
        </div>
        <div class="col-md-6 mb-4">
          <div class="p-4">
            <!--<div class="mb-3">
              <a href="">
                <span class="badge purple mr-1">Category 2</span>
              </a>
              <a href="">
                <span class="badge blue mr-1">New</span>
              </a>
              <a href="">
                <span class="badge red mr-1">Bestseller</span>
              </a>
            </div>-->
            <p class="lead">
              <span>$<?php echo $precio; ?> USD</span>
            </p>
            <p class="lead font-weight-bold">Descripción:</p>
            <p><?php echo $descripcion; ?></p>
            <p>Existencias: <?php echo $existencia; ?></p>
            <!--<form class="d-flex justify-content-left">
              <input type="number" value="1" aria-label="Search" class="form-control" style="width: 100px">
              <button class="btn btn-primary btn-md my-0 p" type="submit">Add to cart
                <i class="fa fa-shopping-cart ml-1"></i>
              </button>
            </form>-->
            <?php if($fb == true){ ?>
              <?php if($existencia > 0){ ?>
                <a href="<?php echo base_url().'productos/buy/'.$id; ?>"><img src="<?php echo base_url(); ?>assets/img/x-click-but01.gif" style="width: 70px;"></a>
                <small class="small pull-right"><?php echo $compras; ?> Compra(s)</small>
              <?php }else{ ?>
                <small>No hay existencias.</small>
              <?php } ?>
            <?php }else{ ?>
              <small>Inicia sesión para comprar este producto.</small>
            <?php } ?>
          </div>
        </div>
      </div>
      <hr>
      <div class="row d-flex justify-content-center wow fadeIn">
        <div class="col-md-6 text-center">
          <h4 class="my-4 h4">Informacion adicional:</h4>
          <p><?php echo $descripcion; ?></p>
        </div>
      </div>
      <!--<div class="row wow fadeIn">
        <div class="col-lg-4 col-md-12 mb-4">
          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/11.jpg" class="img-fluid" alt="">
        </div>
        <div class="col-lg-4 col-md-6 mb-4">
          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/12.jpg" class="img-fluid" alt="">
        </div>
        <div class="col-lg-4 col-md-6 mb-4">
          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/13.jpg" class="img-fluid" alt="">
        </div>
      </div>-->
    </div>
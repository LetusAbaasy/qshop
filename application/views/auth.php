<div class="container">
	<div class="row">
			<?php
			if(!empty($authUrl)) { ?>
			    <div class="col-12">
		        	<div class="">
		          		<a href="<?php echo $authUrl; ?>" class="btn btn-lg btn-primary btn-block"><i class="fa fa-facebook" aria-hidden="true"></i> Iniciar con Facebook</a>
			      	</div>
			    </div>
				<?php }else{ ?>
		    		<div class="col-12">
						<img class="img-thumbnail img-responsive" src="<?php echo $userData['picture_url']; ?>" alt="Imagen del Usuario" width="50" height="50"/>
						<h4>Nombre y apellido:</h4>
						<label><?php echo $userData['first_name'].' '.$userData['last_name']; ?></label>
						<h4>Correo electronico:</h4>
						<label><?php echo $userData['email']; ?></label>
						<div class="form-group">
							<hr/>
							<a href="<?php echo base_url('tienda'); ?>"><button class="btn btn-indigo btn-sm pull-left">Ir a la tienda <i class="fa fa-shopping-basket" aria-hidden="true"></i></button></a>
	    					<button class="btn btn-danger btn-sm pull-right" id="salirFacebook">Cerrar Sesión <i class="fa fa-window-close-o" aria-hidden="true"></i></button>
						</div>
					</div>
					<!--<div class="col-4">
						<h4>Amigos que les gusta QShop:</h4>
						<?php
							for ($i=0; $i < count($userData['friends']); $i++) { 
								echo "<a target='blank_' href=http://www.facebook.com/".$userData['friends'][$i]['id'].">".$userData['friends'][$i]['name']."</a> - ";
							}
					 	?>
				    </div>
				    <?php if(count($userData['likes']) > 0){ ?>
				    <div class="col-4">
				    	<h4>Lista de tus "Me Gusta":</h4>
						<?php 
							for ($i=0; $i < count($userData['likes']); $i++) { 
								echo "<a target='blank_' href='http://www.facebook.com/".$userData['likes'][$i]['id']."'>".$userData['likes'][$i]['name']."</a> - ";
							}
					 	?>
				    </div>-->
				    <div class="clearfix"></div>
				    <hr/>
				    <?php } ?>
				    <!--<div id="" class="">
				    	<h4>Historial de Cupones Comprados:</h4>
                        <div class="">
				    	<?php
				    		foreach ($cupones as $cupon) {
				    		    $amigo = $cupon->amigo;
                                echo '<div class="col-4 card mb-r wow fadeIn">';
                                    echo '<div class="image">';
                                        if($amigo != null || $amigo != ""){
                                            echo '<img class="img-fluid" src="'.base_url('assets/img/fbb.png').'">';
                                        }else{
                                            echo '<img class="img-fluid" src="'.base_url('assets/img/image.png').'">';
                                        }
                                    echo '</div>';
                                    echo '<div class="card-body">';
                                        echo "<label>Serial:</label> <p/>".$cupon->serial."</p>";
                                        echo "<label>Estado:</label> <p>".$cupon->status."</p>";
                                        if($amigo != null || $amigo != ""){
                                            echo "<label>Amigo:</label><p>".$amigo."</p>";
                                        }else{
                                            echo "<label>Amigo:</label><p>No fue seleccionado</p>";
                                        }
                                    echo '</div>';
				    			echo "</div>";
				    		}
				    	?>
				    	</div>
				    </div>-->
				    <div class="col-12">
				    	<div class="clearfix"></div>
                    	<hr/>
				    	<h4>Historial de Compras:</h4>
                        <div class="">
				    	<?php
					    	if($compras){
					    		foreach ($compras as $compra) {
					    			echo '<div class="col-4 card mb-r wow fadeIn">';
	                                    echo '<div class="image">';
	                                      echo '<img class="img-fluid" src="'.base_url('assets/img/paypal.png').'">';
	                                    echo '</div>';
	                                echo '<div class="card-body">';
	                                    echo "<label>ID:</label><p>".$compra->product_id."</p>";
	                                    echo "<label>txn_id:</label><p>".$compra->txn_id."</p>";
	                                    echo "<label>payment_gross:</label><p>".$compra->payment_gross."</p>";
	                                    echo "<label>currency_code:</label><p>".$compra->currency_code."</p>";
	                                    echo "<label>payer_email:</label><p>".$compra->payer_email."</p>";
	                                    echo "<label>payment_status:</label><p>".$compra->payment_status."</p>";
					    			echo '</div>';
					    			echo "</div>";
					    		}
					    	}else{
					    		echo '<p>Ninguna compra realizada.</p>';
					    	}

				    	?>
				    	</div>
				    </div>
				<div class="d-none">
					<?php echo $userData['oauth_uid']; ?>
					<?php echo $userData['gender']; ?>
					<?php echo $userData['locale']; ?>
				</div>
			<?php } ?>
	</div>
</div>
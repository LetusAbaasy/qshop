<!-- Template de Correo de contacto -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/styles.css')?>" rel="stylesheet" type="text/css" />
    <!-- Scripts -->
    <script src="<?php echo base_url('assets/js/jquery-3.1.1.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
    <!-- Title -->
    <title>QShop</title>
</head>
<section>
    <div class="container">
        <div class="row">
            <div>
                <a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/hdr_pg.png"></a>
            </div>
        </div>
    </div>
</section>
<body class="nav-md">
    <div class="container body">
	   	<div class="row" role="main">
        <div id="contenido_">
        </div>
            <div id="contenido_mensaje">
                <?php echo $mensaje; ?>
            </div>
        </div>
    </div>
</body>
<footer>
    <div class="container">
        <div class="row">
            <div>
                <a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/ftr_pg.png"></a>
            </div>
        </div>
    </div>
</footer>
</html>

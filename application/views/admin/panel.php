<div class="container">
	<div class="row">
		<h4>Ingresar nuevo producto:</h4>
		<div class="col-md-12">
		<hr/>
		<?php echo form_open('', array('id' => 'formulario_producto')); ?>
			<div class="form-group">
				<label for="nombreProducto">Nombre:*</label>
				<input type="text" id="nombreProducto" name="nombreProducto" class="form-control" placeholder="Nombre del producto..." required="">
			</div>
			<div class="form-group">
				<label for="precioProducto">Precio:*</label>
				<input type="number" id="precioProducto" name="precioProducto" class="form-control" placeholder="Precio..." required="">
			</div>
			<div class="form-group">
				<label for="descripcionProducto">Descripción:*</label>
				<textarea id="descripcionProducto" name="descripcionProducto" class="form-control" placeholder="Descripción...">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod  incididunt ut labore et doloresa dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non , sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
			</div>
			<div class="form-group">
				<label for="existencia">Existencia:*</label>
				<input type="number" id="existencia" name="existencia" class="form-control" placeholder="Existencia...">
			</div>
			<div class="form-group">
				<label for="imagenProducto">Imagen:*</label>
				<input type="file" id="imagenProducto" name="imagenProducto" class="form-control">
			</div>
		</form>
			<button class="btn btn-success btn-block" id="crearProducto">Crear producto</button>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<hr/>
<div class="col-md-12">
	<h3>Productos en venta</h3>
	<table id="tablaProductos" width="100%" border=0 class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Precio</td>
                <td>Descripción</td>
                <td>Existencias</td>
                <td>Compras</td>
                <td>Acción</td>
            </tr>
        </thead>
        <tbody id="tbody">
        	<?php
                foreach ($productos as $producto) {
	                echo "<tr><td>$producto->nombre</td>";
	                echo "<td>$producto->precio</td>";
	                echo "<td>$producto->descripcion</td>";
	                echo "<td>$producto->existencia</td>";
	                echo "<td>$producto->compras</td>";
	                echo "<td><button class='btn btn-warning' data-id='$producto->id' title=''><i class='fa fa-file' aria-hidden='true'></i></button><button class='btn btn-danger eliminar' data-id='$producto->id' title=''><i class='fa fa-times' aria-hidden='true'></i></button></td></tr>";
	            }
            ?>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
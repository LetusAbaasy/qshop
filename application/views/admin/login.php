<div class="col-md-12">
	<div class="card col-md-3 d-block mx-auto">
	    <div class="card-body">
	        <!--Header-->
	        <div class="form-header deep-blue-gradient rounded">
	            <h3><i class="fa fa-lock"></i> Ingreso de administradores:</h3>
	        </div>
	        <!-- Material input email -->
	        <div class="md-form font-weight-light">
	            <i class="fa fa-envelope prefix grey-text"></i>
	            <input type="email" id="materialFormEmailEx" class="form-control">
	            <label for="materialFormEmailEx" class="">Correo electronico</label>
	        </div>
	        <!-- Material input password -->
	        <div class="md-form font-weight-light">
	            <i class="fa fa-lock prefix grey-text"></i>
	            <input type="password" id="materialFormPasswordEx" class="form-control">
	            <label for="materialFormPasswordEx">Contraseña</label>
	        </div>

	        <div class="text-center mt-4">
	            <a href="<?php echo base_url("administrador/panel"); ?>"><button class="btn btn-light-blue waves-effect waves-light" type="submit">Ingresar <i class="fa fa-caret-square-o-right" aria-hidden="true"></i></button></a>
	        </div>

	    </div>
	    <!--Footer-->
	    <!--<div class="modal-footer">
	        <div class="options font-weight-light">
	            <p>Not a member? <a href="#">Sign Up</a></p>
	            <p>Forgot <a href="#">Password?</a></p>
	        </div>
	    </div>-->
	</div>
</div>
<br/>
<a id="back-to-top" class="btn btn-default back-to-top" role="button" title="Volver arriba" data-placement="right"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
<!--Footer-->
    <footer class="page-footer center-on-small-only mt-0 mdb-color">

        <!--Footer links-->
        <div class="container">
            <div class="row mt-4">
                <!--First column-->
                <div class="col-md-3">
                    <p>Q'Shop</p>
                    <p>Ayudamos a todos..</p>
                </div>
                <!--/.First column-->
                <hr class="w-100 clearfix d-sm-none">
                <!--Second column-->
                <div class="col-md-6 ml-auto">
                    <h5 class="title mb-3">
                        <strong>Comparte...</strong>
                    </h5>
                    <ul>
			            <li><div  class="fb-like" data-share="true" data-width="450" data-show-faces="true"></div></li>
                        <!--<li>
                            <a href="#!">Link 1</a>
                        </li>
                        <li>
                            <a href="#!">Link 2</a>
                        </li>
                        <li>
                            <a href="#!">Link 3</a>
                        </li>
                        <li>
                            <a href="#!">Link 4</a>
                        </li>-->
                    </ul>
                </div>
                <!--/.Second column-->
                <hr class="w-100 clearfix d-sm-none">
                <!--Third column-->
                <!--<div class="col-lg-2 col-md-6 ml-auto">
                    <h5 class="title mb-3">
                        <strong>Second column</strong>
                    </h5>
                    <ul>
                        <li>
                            <a href="#!">Link 1</a>
                        </li>
                        <li>
                            <a href="#!">Link 2</a>
                        </li>
                        <li>
                            <a href="#!">Link 3</a>
                        </li>
                        <li>
                            <a href="#!">Link 4</a>
                        </li>
                    </ul>
                </div>-->
                <!--/.Third column-->
                <hr class="w-100 clearfix d-sm-none">
                <!--Fourth column-->
                <div class="col-md-3">
                    <h5 class="title mb-3">
                        <strong>Administración</strong>
                    </h5>
                    <ul>
                        <li>
                            <a class="white-text waves-effect waves-light" href="<?php echo base_url('administrador'); ?>">Ingreso de administradores</a>
                        </li>
                    </ul>
                </div>
                <!--/.Fourth column-->
            </div>
        </div>
        <!--/.Footer links-->
        <!--Copyright-->
        <div class="footer-copyright">
            <div class="container-fluid">
               <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="copyright">
						© <?php echo date('Y'); ?>, QShop, Todos los derechos reservados.
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="design">
						 <a href="<?php echo base_url(); ?>">QShop</a> |  <a target="_blank" href="https://get.adobe.com/es/flashplayer/">Web Design & Desarrollo por Sergio Martínez y Equipo.</a>
					</div>
				</div>
            </div>
        </div>
        <!--/.Copyright-->

    </footer>
	<div class="hidden" id="assets">
	    <!-- Styles -->
	    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
	    <!-- Material Design Bootstrap -->
        <link href="<?php echo base_url('assets/css/mdb.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/animate.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/styles.css')?>" rel="stylesheet">
	    <!-- Scripts -->
        <script src="<?php echo base_url('assets/js/modernizr.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/jquery-3.1.1.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/mdb.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/popper.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/notifIt.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery.validate.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/script.js')?>" type="text/javascript"></script>
        <!-- Font Awesome -->
	    <script type="text/javascript">
		  window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1584590751598884',
		      xfbml      : true,  
		      version    : 'v2.10'
		    });
		    FB.AppEvents.logPageView();
		  };

		  (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/en_US/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>
	</div>
</body>
</html>
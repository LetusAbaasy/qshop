<!DOCTYPE html>
<html lang="es" class="full-height">
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta http-equiv="refresh" content="7200" />
  <meta name="application-name" content="" />
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="" />
  <meta name="revisit-after" content="30 days" />
  <meta name="distribution" content="web" />
  <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW" />
  <link href="<?php echo base_url('assets/img/favicon.ico')?>" type="image/x-icon" rel="icon" />
  <meta name="theme-color" content="#000000"/>
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="white-translucent" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119699965-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-119699965-1');
  </script>
    <!-- Title -->
	<title>QShop | <?php echo $title; ?></title>
</head>
<body class="nav-md">
    <header id="header">
      <!--Navbar-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top scrolling-navbar">
            <div class="container">
                <a class="navbar-brand font-bold dark-grey-text" href="<?php echo base_url('home'); ?>">
                    <strong>Q'Shop</strong>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link dark-grey-text waves-effect waves-light" href="<?php echo base_url('home'); ?>">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item pull-right">
                            <a class="nav-link dark-grey-text waves-effect waves-light" href="<?php echo base_url('registro'); ?>">Registro
                                <span class="sr-only">Registro</span>
                            </a>
                        </li>
                        <li class="nav-item pull-right">
                            <a class="nav-link dark-grey-text waves-effect waves-light" href="<?php echo base_url('tienda'); ?>">Tienda
                                <span class="sr-only">Tienda</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Intro Section -->
        <div class="view hm-white-light jarallax" style="background-image: url('<?php echo base_url('assets/img/banner2.jpg'); ?>');">
            <div class="full-bg-img">
                <div class="container flex-center">
                    <div class="row pt-5 mt-3">
                        <div class="col-md-12 mb-3">
                            <div class="intro-info-content text-center">
                                <img class="img-responsive" src="<?php echo base_url('assets/img/QShop.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <hr/>
    <br/>
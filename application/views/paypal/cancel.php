<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron text-center">
			  <h2>Lo sentimos, la transacción ha sido cancelada.</h2>
			  <p><a class="btn btn-primary btn-block" href="<?php echo base_url('tienda'); ?>" role="button"><i class="fa fa-chevron-left" aria-hidden="true"></i> Volver a QShop</a></p>
			</div>
	    </div>
	</div>
</div>

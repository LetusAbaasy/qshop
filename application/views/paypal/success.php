<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="jumbotron text-center">
          <h2>Gracias, por comprar nuestros productos.</h2>
          <h3>Numero del producto:</h3>
          <p id="item_number"><?php echo $item_number; ?></p>
          <h3>TXN ID:</h3>
          <p id="txn_id"><?php echo $txn_id; ?></p>
          <h3>Monto pagado:</h3>
          <p id="pago"><?php echo $payment_amt.' - '.$currency_code; ?></p>
          <h3>Estado del pago:</h3>
          <p id="status">Pagado</p>
          <!--<h3>¿Desea regalar el cupón a sus amigos?</h3>
          <select id="amigosRegalar" class="form-control selectpicker">
            <?php 
              for ($i=0; $i < count($userData['friends']); $i++) { 
                echo "<option data-email='".$userData['email']."' id='".$userData['friends'][$i]['id']."'>".$userData['friends'][$i]['name']."</option>";
              }
            ?>
          </select>-->
          <!--<button class="btn btn-success enviarCorreo" data-email="<?php echo $userData['email']; ?>" id="enviarCupon">Enviar Cupón <i class="fa fa-tags" aria-hidden="true"></i></button>
          <hr/>-->
          <a class="btn btn-primary btn-block enviarCorreo" data-email="<?php echo $userData['email']; ?>" href="<?php echo base_url('tienda'); ?>" role="button"><i class="fa fa-chevron-left" aria-hidden="true"></i> Volver a QShop</a>
          </p>
        </div>
      </div>
  </div>
</div>

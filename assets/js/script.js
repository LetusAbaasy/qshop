$( document ).ready(function(){
    new WOW().init();
    back_to_top();

    var hashUrl = window.location.hash;
    var appUrl = unescape(window.location.href);
    var url = unescape(window.location.href);
    var appUrl = appUrl.split('/');
    //var baseURL = appUrl[0]+'//'+appUrl[2]+'/';
    var baseURL = appUrl[0]+'//'+appUrl[2]+'/'+appUrl[3]+'/'+appUrl[4]+'/';
    var $funcion = appUrl[4];
    var funcion_ = appUrl[5];



    $("#crearProducto").click(function(){
        if($("#formulario_producto").valid()){
            $nombreProducto = $("#nombreProducto").val();
            $precioProducto = $("#precioProducto").val();
            $descripcionProducto = $("#descripcionProducto").val();
            $existencia = $("#existencia").val();

            var file_data = $('#imagenProducto').prop('files')[0];   
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('nombreProducto', $nombreProducto);              
            form_data.append('precioProducto', $precioProducto);                 
            form_data.append('descripcionProducto', $descripcionProducto);
            form_data.append('existencia', $existencia);

            $.ajax({
                url: baseURL+'/crearProducto',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                dataType: "JSON",
                success: function(response){
                    console.log(response);
                    alert(response.msg);
                    notificacion(response.msg, "success");
                    setInterval("redirect('"+baseURL+"/panel')", 1000);
                },
                error: function(ev){
                    //Do nothing
                }
             });
        }
    });

    $("#salirFacebook").click(function(){
        redirect(baseURL+"logout");
    });

    $(".eliminar").click(function(){
        console.log("asdsa");
        $id = $(this).attr("data-id");

        data = {
            'id': $id
        }

        $.ajax({
            url: baseURL+"/eliminarProducto",
            type: "post",
            dataType: "JSON",
            data: data,
            beforeSend: function(){ 
                notificacion("Espere...", "success");
            },
            success: function (response) {
                notificacion(response.msg, "success");
                alert(response.msg);
                $(this).parent().remove();
                setInterval("redirect('"+baseURL+"/panel')", 1000);
            },
            error: function(ev){
                console.error(ev);
            }
        });
    });


    $(".enviarCorreo").click(function(){
        $email = $(this).attr("data-email");
        $item_number = $("#item_number").html();
        $amigo = $("#amigosRegalar").val();
        $txn_id = $("#txn_id").html();
        $status = $("#status").html();

        $pago = $("#pago").html();
        $split = $pago.split('-');
        $payment_amt = $split[0];
        $currency_code = $split[1];
        

        data = {
            'email': $email,
            'amigo': $amigo,
            'item_number': $item_number,
            'txn_id': $txn_id,
            'status': $status,
            'payment_amt': $payment_amt,
            'currency_code': $currency_code
        }

        $.ajax({
            url: baseURL+"home/email",
            type: "post",
            dataType: "JSON",
            data: data,
            beforeSend: function(){ 
                notificacion("Espere...", "success");
            },
            success: function (response) {
                notificacion(response.msg, "success");
                alert(response.msg);
                setInterval("redirect('"+baseURL+"')", 4000);
            },
            error: function(ev){
                console.error(ev);
            }
        });
    });




    $("form[id='formulario_producto']").validate({
        rules: {
          nombreProducto: {
            required: true,
            minlength: 3,
          },
          precioProducto: {
            required: true,
            number: true
          },
          descripcionProducto: {
            required: true,
          },
          existencia: {
            required: true,
            number: true
          }
        },
        messages: {
          nombreProducto: {
            required: "Por favor, escriba el nombre del producto.",
            minlength: "El nombre del producto debe tener mínimo 3 caracteres.",
          },
          precioProducto: {
            required: "Por favor, escriba el precio en dolares 12.25.",
            number: "Tienen que ser solamente numeros."
          },
          descripcionProducto: {
            required: "Por favor, escriba la descripcion del producto.",
          },
          existencia: {
            required: "Por favor, escriba la contraseña.",
            number: "Tienen que ser solamente numeros."
          }
        }
    });
});

function redirect(response){
    $url = response.replace('"','').replace('"','');
    $(window).attr("location", $url);
}

/**
    Recargar la pagina, en false para cache, en true para cargar desde 0.
**/
function reload(){
    location.reload(false);
}

function back_to_top(){
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
}

function notificacion($msg, $type){
    notif({
        type: $type,
        msg: $msg,
        position: "right",
        width: 350,
        height: 60,
        autohide: false,
        multiline: true,
        fade: true,
        bgcolor: "#0e3b5e",
        color: "#fff",
        opacity: 1,
    });
}